package dark1;


import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;

import java.awt.*;



public class RobotFim extends AdvancedRobot {

	public void run() {
		// cores
		setBodyColor(Color.black);
		setGunColor(Color.green);
		setRadarColor(Color.black);

		// enquanto(loop)
		while (true) {
			setTurnRight(200);
			setMaxVelocity(5);
			ahead(300);
		}
	}

	// quando encontrar um oponente, atirar se...
	
	public void onScannedRobot(ScannedRobotEvent oponente) {
	double ang = oponente.getBearing();
	double dist = oponente.getDistance();
	   if (dist <200) { 
	   turnGunRight(ang);
		fire(2);
		}
		else {
			fire(1);
			}
		}

	//se colidir com outro robô...
	
	public void onHitRobot(HitRobotEvent e) {
		if (e.getBearing() > -10 && e.getBearing() < 10) {
			fire(3);
		}
		if (e.isMyFault()) {
			turnRight(10);
		}
	
  }
}
